import Vue from 'vue'
import vuexI18n from 'vuex-i18n'

import store from './store'

import english from '../lang/en'
import german from '../lang/de'

Vue.use(vuexI18n.plugin, store)

Vue.i18n.add('en', english)
Vue.i18n.add('de', german)

Vue.i18n.set('de')