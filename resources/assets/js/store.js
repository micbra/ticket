import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'

import Sorting from './sorting'

const csrf_token = document.head.querySelector('meta[name="csrf-token"]').content

axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-Token': csrf_token
}

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        tickets: [],
        user: {},
        settings: {},
        team: [],
        subscription: {}
    },
    mutations: {
        userData(state, data) {
            state.user = data
        },
        subscriptionData(state, data) {
            Vue.set(state, 'subscription', data)
        },
        activatePlan(state, plan) {
            Vue.set(plan, 'is_active', true)
            Vue.set(plan, 'activated', moment().format('DD.MM.YYYY'))
            Vue.set(state.subscription, 'data', plan)
            
        },
        cancelPlan(state, { plan, ends_at}) {
            Vue.set(plan, 'is_canceled', true)
            Vue.set(plan, 'expires', moment(ends_at, 'YYYY-MM-DD HH:mm:ss').format('DD.MM.YYYY'))
        },
        resumePlan(state, plan) {
            Vue.set(plan, 'is_canceled', false)
            Vue.set(plan, 'expires', null)
        },
        upgradePlan(state, plan) {
            state.subscription.plans.map(plan => {
                plan.is_active = false
                plan.is_canceled = false
                plan.expires = null
                plan.activated = null
            })

            Vue.set(plan, 'is_active', true)
            Vue.set(plan, 'activated', moment().format('DD.MM.YYYY'))            
        },
        updateSettings(state, data) {
            Vue.set(state, 'settings', data)
        },
        fetchData(state, { route, data }) {
            if (route === 'tickets' || route === 'settings') {
                Vue.set(state, 'tickets', data)
            } else {
                state.tickets.push(data[0])
            }
        },
        fetchUsers(state, data) {
            Vue.set(state, 'users', data)
        },
        fetchTeam(state, data) {
            Vue.set(state, 'team', data)
        },
        createTeam(state, data) {
            state.team.push(data)
        },
        addTeamuser(state, data) {
            state.team[0].invited.push(data)
        },
        removeTeamuser(state, data) {
            Vue.set(state.team[0], 'users', data)
        },
        revokeInvitation(state, data) {
            Vue.set(state.team[0], 'invited', data)
        },
        deleteTeam(state) {
            state.team.splice(0,1)
        },
        changeAvatar(state, data) {
            Vue.set(state.settings, 'avatar', data)
        },
        deleteAvatar(state) {
            state.settings.avatar = null
        },
        createTicket(state, data) {
            data.items = []
            state.tickets.push(data)
        },
        updateTicket(state, { ticket, data }) {
            ticket = Object.assign({}, ticket, data)
        },
        deleteTicket(state, index) {
            state.tickets.splice(index, 1)
        },
        toggleDone(state, {ticket, item}) {
            Vue.set(item, 'done', !item.done)
            
            ticket.items.sort(Sorting.sortByDate).sort(Sorting.sortByDone)
        },
        createItem(state, {ticket, data}) {
            data.comments = []
            ticket.items.push(data)
            ticket.items.sort(Sorting.sortByDate).sort(Sorting.sortByDone)
        },
        deleteItem(state, {ticket, index}) {
            if (index !== -1) {
                ticket.items.splice(index, 1)
            }
        },
        updateItem(state, { ticket, item, itemIndex, data}) {
            const mergedItem = Object.assign({}, item, data)
            Vue.set(ticket.items, itemIndex, mergedItem)
            ticket.items.sort(Sorting.sortByDate).sort(Sorting.sortByDone)
        },
        createComment(state, {item, data}) {
            item.comments.push(data)
        },
        updateComment(state, {comment, data}) {
            comment = Object.assign({}, comment, data)
        },
        deleteComment(state, {item, index}) {
            if (index !== -1) {
                item.comments.splice(index, 1)
            }
        }
    },
    actions: {
        activatePlan(state, {stripe_plan, quantity}) {
            const plan = state.getters.getPlan(stripe_plan, quantity)
            state.commit('activatePlan', plan)
        },
        cancelPlan(state, { stripe_plan, quantity, ends_at }) {
            const plan = state.getters.getPlan(stripe_plan, quantity)
            state.commit('cancelPlan', {plan, ends_at})
        },
        resumePlan(state, { stripe_plan, quantity }) {
            const plan = state.getters.getPlan(stripe_plan, quantity)
            state.commit('resumePlan', plan)
        },
        upgradePlan(state, { stripe_plan, quantity }) {
            const plan = state.getters.getPlan(stripe_plan, quantity)
            state.commit('upgradePlan', plan)
        },
        async changeAvatar(state, data) {
            const response = await axios.post('/api/settings', data)

            if (response.status === 200) {
                state.commit('changeAvatar', response.data.avatar)
            }
        },
        async deleteAvatar(state) {
            const response = await axios.delete('/api/settings')
            if (response.status === 200) {
                state.commit('deleteAvatar')
            }
        },
        async toggleDone(state, {data, payload}) {
            const url = `/api/tickets/${data.ticket_id}/items/${data.id}`
            const request = await axios.put(url, { done: payload })

            const ticket = state.getters.getTicket(data.ticket_id)
            const item = state.getters.getItem(ticket, data.id)
            
            if (request.status === 200) {
                state.commit('toggleDone', {ticket, item})
            }
        },
        async createTicket(state, data) {
            const payload = {
                title: data.title,
                private: data.private
            }

            const request = await axios.post('/api/tickets', payload).catch(e => console.error(e))
            if (request.status === 200) {
                state.commit('createTicket', request.data)
                return request.data.id
            }
        },
        async updateTicket(state, data) {
            const payload = {
                title: data.title,
                private: data.private
            }
            const request = await axios.put(`/api/tickets/${data.id}`, payload).catch(e => console.error(e))
            
            const ticket = state.getters.getTicket(data.id)
            
            if (request.status === 200) {
                state.commit('updateTicket', { ticket, data })
            }
        },
        async deleteTicket(state, data) {
            const url = `/api/tickets/${data}`
            const request = await axios.delete(url).catch(e => console.error(e))

            const index = state.getters.getTicketIndex(data)
            
            if (request.status === 200) {
                state.commit('deleteTicket', index)
                return true
            }
        },
        async createItem(state, data) {
            const payload = {
                'title': data.title,
                'due_date': data.due_date,
                'description': data.description,
                'assignee': data.user_id,
                'done': false
            }
            const request = await axios.post(`/api/tickets/${data.ticket_id}/items`, payload).catch(e => console.error(e))

            const ticket = state.getters.getTicket(data.ticket_id)

            if (request.status === 200) {
                state.commit('createItem', {ticket, data: request.data})
                return request.data.id
            }
        },
        async deleteItem(state, data) {
            const url = `/api/tickets/${data.ticket_id}/items/${data.id}`
            const request = await axios.delete(url).catch(e => console.error(e))

            const ticket = state.getters.getTicket(data.ticket_id)
            const index = state.getters.getItemIndex(ticket, data.id)

            if (request.status === 200) {
                state.commit('deleteItem', {ticket, index})
            }
        },
        async updateItem(state, {data, date}) {
            const payload = {
                'title': data.title,
                'due_date': date,
                'description': data.description,
                'assignee': data.user_id,
                'done': data.done
            }
            const request = await axios.put(`/api/tickets/${data.ticket_id}/items/${data.id}`, payload).catch(e => console.error(e))

            const ticket = state.getters.getTicket(data.ticket_id)
            const item = state.getters.getItem(ticket, data.id)
            const itemIndex = state.getters.getItemIndex(ticket, data.id)
            
            if (request.status === 200) {
                state.commit('updateItem', { ticket, item, itemIndex, data: request.data})
            }
        },
        async createComment(state, {data, payload}) {
            const url = `/api/tickets/${data.ticket_id}/items/${data.id}/comments`
            const request = await axios.post(url, { body: payload }).catch(e => console.error(e))

            const ticket = state.getters.getTicket(data.ticket_id)
            const item = state.getters.getItem(ticket, data.id)

            if (request.status === 200) {
                state.commit('createComment', {item, data: request.data})
            }
        },
        async updateComment(state, {data, ticketId}) {
            const url = `/api/tickets/${ticketId}/items/${data.item_id}/comments/${data.id}`
            const request = await axios.put(url, { 'body': data.body }).catch(e => console.error(e))

            const ticket = state.getters.getTicket(ticketId)
            const item = state.getters.getItem(ticket, data.item_id)
            const comment = state.getters.getComment(item, data.id)

            if (request.status === 200) {
                state.commit('updateComment', {comment, data: request.data})
            }
        },
        async deleteComment(state, { data, ticketId }) {
            const url = `/api/tickets/${ticketId}/items/${data.item_id}/comments/${data.id}`
            const request = await axios.delete(url).catch(e => console.error(e))

            const ticket = state.getters.getTicket(ticketId)
            const item = state.getters.getItem(ticket, data.item_id)
            const index = state.getters.getCommentIndex(item, data.id)
            
            if (request.status === 200) {
                state.commit('deleteComment', { item, index })
            }
        }
    },
    getters: {
        getTickets(state) {
            if (state.tickets[0] !== undefined) {
                return state.tickets
            }
            return null
        },
        getTicket(state) {
            return id => state.tickets.find(ticket => ticket.id == id)
        },
        getTicketIndex(state) {
            return id => state.tickets.findIndex(ticket => ticket.id == id)
        },
        getItem(state) {
            return (ticket, itemId) => ticket.items.find(item => item.id == itemId)
        },
        getItemIndex(state) {
            return (ticket, itemId) => ticket.items.findIndex(item => item.id == itemId)            
        },
        getComment(state) {
            return (item, commentId) => item.comments.find(comment => comment.id == commentId)
        },
        getCommentIndex(state) {
            return (item, commentId) => item.comments.findIndex(comment => comment.id == commentId)
        },
        getItemsByUser(state) {
            return userId => {
                const arr = state.tickets.map(ticket => ticket.items)
                const flat = [].concat.apply([], arr)
                return flat.filter(item => item.user_id === userId)
            }
        },
        getAvatar(state) {
            return userId => {
                if (state.user.id === userId) {
                    return state.user.settings.avatar
                } else if (state.team[0]) {
                    return state.team[0].users.find(member => member.id === userId).settings.avatar
                } else {
                    return null
                }
            }
        },
        getPlan(state) {
            return (planId, quantity) => state.subscription.plans.find(plan => plan.plan_id === planId && plan.users_included === quantity)
        }
    }
})