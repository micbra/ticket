require('./bootstrap');

import Vue from 'vue'
import router from './router'
import store from './store'
import i18n from './i18n'
import App from '../components/App.vue'

window.eventbus = new Vue()

const app = new Vue({
    el: '#root',
    render: h => h(App),
    router,
    store
})
