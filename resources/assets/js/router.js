// 3rd party
import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'

import moment from 'moment'

import store from './store'

// components
import TicketOverview from '../components/TicketOverview.vue'
import TicketDetail from '../components/TicketDetail.vue'
import CreateTicket from '../components/CreateTicket.vue'
import EditTicket from '../components/EditTicket.vue'
import CreateItem from '../components/CreateItem.vue'
import EditItem from '../components/EditItem.vue'
import Settings from '../components/Settings.vue'
import Subscription from '../components/Subscription.vue'
import Team from '../components/Team.vue'
import Help from '../components/Help.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/tickets',
            component: TicketOverview,
            name: 'tickets'
        },
        {
            path: '/tickets/create',
            component: CreateTicket,
            name: 'create-ticket'
        },
        {
            path: '/tickets/:ticketId',
            component: TicketDetail,
            name: 'ticket-detail'
        },
        {
            path: '/tickets/:ticketId/edit',
            component: EditTicket,
            name: 'edit-ticket'
        },
        {
            path: '/tickets/:ticketId/items/',
            redirect: '/tickets/:ticketId'
        },
        {
            path: '/tickets/:ticketId/items/create',
            component: CreateItem,
            name: 'create-item'
        },
        {
            path: '/tickets/:ticketId/items/:itemId',
            component: TicketDetail,
            name: 'view-item'
        },
        {
            path: '/tickets/:ticketId/items/:itemId/edit',
            component: EditItem,
            name: 'edit-item'
        },
        {
            path: '/tickets/:ticketId/items/:itemId/comments',
            redirect: '/tickets/:ticketId/items/:itemId'
        },
        {
            path: '/tickets/:ticketId/items/:itemId/comments/:commentId',
            redirect: '/tickets/:ticketId/items/:itemId'
        },
        {
            path: '/settings',
            component: Settings,
            name: 'settings'
        },
        {
            path: '/team',
            component: Team,
            name: 'team'
        },
        {
            path: '/subscription',
            component: Subscription,
            name: 'subscription'
        },
        {
            path: '/help',
            component: Help,
            name: 'help'
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

router.beforeEach(async (to, from, next) => {
    if (!store.state.tickets.length) {
        const ticketId = to.params.ticketId ? `/${to.params.ticketId}` : ''
        const { data } = await axios.get('/api/tickets' + ticketId).catch(e => console.error(e))
        if (data.length) {
            store.commit('fetchData', { route: to.name, data })
        }
    }

    if (!store.state.user.id) {
        const { data } = await axios.get('/api/user').catch(e => console.error(e))
        store.commit('userData', data)
    }

    if (!store.state.team.length) {
        const { data } = await axios.get('/api/team').catch(e => console.error(e))
        store.commit('fetchTeam', data)
    }

    if (!store.state.settings.id) {
        const { data } = await axios.get('/api/settings').catch(e => console.error(e))
        store.commit('updateSettings', data)

        // i18n
        const language = store.state.settings.language
        Vue.i18n.set(language)
        moment.locale(language)
    }

    if (!Object.keys(store.state.subscription).length) {
        const { data } = await axios.get('/api/subscription').catch(e => console.error(e))
        store.commit('subscriptionData', data)
    }
    
    next()

})

export default router