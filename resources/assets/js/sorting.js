export default class Sorting {
    static sortByDate(a, b) {
        const x = new Date(a.due_date)
        const y = new Date(b.due_date)

        if (a.due_date == null || b.due_date == null) {
            return 0
        } else if (x === y) {
            return 0
        } else if (x > y) {
            return 1
        } else {
            return -1
        }
    }

    static sortByDone(a, b) {
         return (a.done === b.done) ? 0 : a.done ? 1 : -1
    }
}