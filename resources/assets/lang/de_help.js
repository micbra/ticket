export default `
# Hilfe zur Bedienung von Tickets
Das Konzept der Tickets-App fußt im Grunde auf drei Prinzipien: **Tickets**, **Einträge** und **Teams**.

## Tickets
Tickets stellen die oberste Ebene der Organisation dar. Sie dienen dazu, mehrere Einträge zu gruppieren.
Erstellen Sie beispielsweise ein Ticket für jedes Projekt, jeden Kunden etc., das/den Sie betreuen.

### Ticket erstellen
Um ein neues Ticket zu erstellen, klicken Sie auf **Ticket erstellen** in der *Tickets* ansicht auf Ihrem [Dashboard](/tickets).

>Sie können beliebig viele Tickets erstellen.

Geben Sie im folgenden Schritt einen Namen für das Ticket ein.

#### Private Tickets
Bei der Ticketerstellung haben Sie die möglichkeit, ein Ticket als privat zu markieren.
Wenn sie den entsprechenden haken setzen, werden nur Sie in der Lage sein, das Ticket zu sehen. Ihre Team-Mitglieder sehen nur öffentliche Tickets!

### Ticket bearbeiten
Um ein Ticket zu bearbeiten, etwa den Namen oder den Privat-Status zu ändern, wählen Sie das Ticket aus und klicken dann oben rechts auf das Stift-Symbol.

### Ticket löschen
Um ein Ticket zu löschen, wählen Sie das Ticket aus und klicken, oben rechts auf das Mülleimer-Symbol und bestätigen das folgende Dialogfeld.

> Achtung: Wenn Sie ein Ticket löschen, werden alle damit verbundenen Einträge sowie deren Kommentare unwiederbringlich gelöscht!

## Einträge
Jedes Ticket kann beliebig viele Einträge enthalten.
Einträge können beispielsweise Aufgaben, die ihn Ihrem Projekt anfallen oder anstehende Termine sein.

### Eintrag erstellen
Um einen Eintrag anzulegen, wählen Sie ein Ticket aus und klicken anschließend auf **Eintrag erstellen**.

Jeder Eintrag muss einen *Titel* haben. Optional können Sie ein *Ablaufdatum* festlegen, eine *Beschreibung* hinzufügen 

> Die Beschreibung eines Eintrags kann mit [Markdown](https://guides.github.com/features/mastering-markdown/) formatiert werden.

oder einen *Benutzer zuweisen*.
> Wenn sie keinen Benutzer zuweisen, wird das Ticket automatisch Ihnen zugewiesen.

### Eintrag ansehen
Nach dem Erstellen eines Eintrags wird Ihnen dieser Standardmäßig aufgeklappt angezeigt. Hier können Sie die Beschreibung sowie die Kommentare sehen (bzw. letztere hinzufügen) und finden mit dem Ketten-Symbol einen permanenten Link zu diesem Eintrag.
Wenn Sie den Eintrag zuklappen oder einen anderen aufklappen möchten, klicken Sie einfach auf den Titel des jeweiligen Eintrags.

#### Eintrags-Priorität
Einträge, die ein festgelegtes Ablaufdatum besitzen, werden, je nachdem wie nahe das Ablaufdatum in der Zukunft steht in grün, gelb oder rot angezeigt.
Standardmäßig werden alle Einträge, deren Ablaufdatum mehr als eine Woche in der Zukunft liegt, grün angezeigt. Liegt es weniger als eine Woche aber mehr als 24 Stunden in der Zukunft, wird es gelb dargestellt und liegt es weniger als 24 Stunden in der Zukunft oder gar in der Vergangenheit, wird der Eintrag in rot markiert.

> Die Standardeinstellungen können Sie im Bereich *Anwendings-Einstellungen* in den [Einstellungen](/settings) anpassen.

### Eintrag erledigen
Wenn Sie einen Eintrag als erledigt kennzeichnen möchten, klicken Sie auf das Haken-Symbol des jeweiligen Eintrags. Er wird nun mit weniger Deckkraft dargestellt und ganz unten ans Ende des Tickets verschoben.

Möchten Sie den Eintrag wieder als nicht erledigt kennzeichnen, klicken Sie einfach wieder auf den gleichen Button (dieser wird nun gelb und mit einem Pfeil-Symbol angezeigt).

### Eintrag bearbeiten
Wenn Sie Daten eines Eintrags ändern oder ihn einer anderen person zuweisen möchten, klicken Sie bitte auf das Stift-Symbol des jeweiligen Eintrags.

### Eintrag löschen
Um einen Eintrag zu löschen, klicken Sie auf das Mülleimer-Symbol des jeweiligen Eintrags und bestätigen das folgende Dialogfeld.

> Achtung: Wenn Sie einen Eintrag löschen, werden neben den Daten (Titel, Beschreibung etc.) alle damit verbundenen Kommentare unwiederbringlich gelöscht!

### Kommentare
Jeder Eintrag kann beliebig viele Kommentare beinhalten.

> Kommentare können mit [Markdown](https://guides.github.com/features/mastering-markdown/) formatiert werden.

Wenn Sie der Ersteller eines Kommentars sind, können Sie ihn bearbeiten oder löschen. Bitte klicken Sie dazu einfach auf das jeweilige Symbol neben dem Kommentar.

## Teams
Jeder Benutzer kann ein Team erstellen. Um jedoch Mitglieder zu einem Team hinzufügen zu können, müssen Sie auf einen bezahlten Tarif upgraden.

> Upgrades können Sie auf der [Abonnement](/subscription)-Seite vornehmen!

Team-Mitglieder sehen jeweils alle öffntlichen Tickets ihres Teams und können diese bearbeiten oder neue Tickets erstellen.
Außerdem können sie Tickets Einträge hinzufügen oder bereits vorhandene bearbeiten oder löschen. Auch kommentieren von Einträgen ist möglich.

### Team erstellen
Um ein Team zu erstellen, müssen Sie lediglich auf die [Team](/team)-Seite wechseln, einen Team-Namen wählen und den **Team-Erstellen** Button klicken.

### Team-Mitglieder
Jedes Team kann maximal die im Abonnement festgelegte Anzahl an Mitgliedern haben.

#### Mitglied hinzufügen
Um ein Mitglied hinzuzufügen, geben Sie seine E-Mail-Addresse in das entsprechende Feld ein und klicken auf **Mitglied hinzufügen**.
Es wird eine Einladung an die angegebene E-Mail-Adresse gesendet, welche nun vom Empfänger angenommen werden kann.

#### Mitglied entfernen / Einladung zurückziehen
Um ein Mitglied zu entfernen bzw. eine gesendete Einladung zurückzuziehen, klicken Sie bitte das Mülleimer-Symbol neben dem jeweiligen Mitglied.
> Beachten Sie, dass sie ggf. gezwungen sind, Team-Mitglieder zu entfernen, wenn Sie Ihren Abonnement-Tarif downgraden, also einen mit weniger Mitgliedern auswählen, als Ihr Team Mitglieder hat.

&nbsp;

# Bekannte Probleme
- Bei einer Neuregistrierung, die über eine Einladungs-E-Mail erfolgt, wird der neue Benutzer nicht zu dem eingeladenen Team hinzugefügt. Der Button zum Annehmen der Einladung in der E-Mail muss nochmals betätigt werden.
- Nach dem Ändern der Zuweisung eines Eintrags wird manchmal weiterhin der alte Benutzer als zugewiesen angezeigt. Es hilft, die Seite neu zu laden.
- Nach dem aktivieren oder wechseln des Abonnements werden Start- oder Ablaufdatum manchmal nicht angezeigt. Es hilft, die Seite neu zu laden.
`