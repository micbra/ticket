import help from './en_help'

export default {
    general: {
        back: 'Back',
        edit: 'Edit {name}',
        save: 'Save',
        confirm: 'Confirm',
        cancel: 'Cancel'
    },
    header: {
        login: 'Login',
        register: 'Register',
        settings: 'Settings',
        team: 'Team',
        subscription: 'Subscription',
        help: 'Help',        
        logout: 'Logout'
    },
    ticket: {
        tasks: 'My Tasks for Today',
        no_tasks: 'No Tasks for today 😊',
        create: 'Create Ticket',
        edit: 'Edit Ticket',
        delete: 'Delete Ticket',
        save: 'Save Ticket',
        delete_title: 'Delete Ticket',        
        delete_message: 'Do you really want to delete "{name}"?',
        empty: 'No Tickets here yet. Feel free to create one.',
        private: 'Private Ticket',
        private_tooltip: 'This ticket is only visible to you',
        critical: '{name} contains Items that reach their due date today!',
        warning: '{name} contains Items that reach their due date within this week!',
        items: 'Items in {name}',
        placeholder: {
            title: 'Ticket Title'
        }
    },
    item: {
        create: 'Create Item',
        actions: 'Show actions…',        
        edit: 'Edit Item',
        delete: 'Delete Item',
        close: 'Close Item',
        open: 'Reopen Item',
        save: 'Save Item',
        delete_title: 'Delete Item',        
        delete_message: 'Do you really want to delete "{name}"?',
        empty: 'No Items on this Ticket. Why don\'t you add some?',
        description: 'Description',
        no_description: 'No description.',
        permalink: 'Permanent link to this item',
        placeholder: {
            title: 'Item Title',
            date: 'Due Date',
            description: 'Description',
            assignee: 'Assign to:'
        }
    },
    comment: {
        edit: 'Edit Comment',
        updated: 'updated {time}',
        delete: 'Delete Comment',
        delete_title: 'Delete Comment',
        delete_message: 'Do you really want to delete this comment?',
        count: '{count} comment ::: {count} comments',
        headline: 'Comments',
        empty: 'No comments yet.',
        save: 'Save Comment',
        placeholder: 'Write a comment…'
    },
    settings: {
        application: 'Application ',
        user: 'User ',
        advanced: 'Advanced',
        critical: 'Days until critical status',
        warning: 'Days until warning status',
        placeholder: 'Days',
        language: 'Language',
        avatar: 'Profile Picture',        
        hide: 'Hide this user in all dropdowns',
        delete_caches_all: 'Delete all caches',
        delete_caches_app: 'Delete app cache',
        delete_caches_api: 'Delete api cache'
    },
    team: {
        button_create: 'Create Team',
        button_add: 'Add Member',
        button_delete: 'Delete Team',
        button_rename: 'Rename Team',
        owner: 'Owner',
        member: 'Member',
        invited: 'Invited',
        label_create: 'Create new Team',
        label_add: 'Add new Team Member',
        placeholder_name: 'Team Name',
        placeholder_email: 'Email Address',
        tooltip_delete: 'Remove Team Member',
        tooltip_revoke: 'Revoke Invitation',        
        delete_team_title: 'Delete Team',
        delete_team_body: 'Are you sure to delete "{team}"?',
        delete_member_title: 'Remove Team Member',
        delete_member_body: 'Are you sure you want to remove {name} from "{team}"?',
        revoke_invitation_title: 'Revoke Invitation',
        revoke_invitation_body: 'Are you sure to revoke the invitation of {name}?'
    },
    subscription: {
        users: 'User',
        month: 'per month',
        year: 'per year',
        monthly: 'monthly',
        yearly: 'yearly',
        payment: 'payment',
        valid_from: 'Valid from:',
        valid_until: 'Valid to:',
        subscribe: 'Subscribe',
        cancel: 'Cancel',
        resume: 'Resume',
        upgrade: 'Upgrade',
        payment_details: 'Please enter your payment details:',
        processing: 'Your transaction is being processed. Please wait',
        messages: {
            active: 'Your payment has been processed. Your subscription is now active!',
            resumed: 'Your subscription has been resumed.',
            canceled: 'Your subscription has been cancled.',
            upgraded: 'Your subscription has been upgraded.'
        }
    },
    help
}