import help from './de_help'

export default {
    general: {
        back: 'Zurück',
        edit: '{name} bearbeiten',
        save: 'Speichern',
        confirm: 'Bestätigen',
        cancel: 'Abbrechen'
    },
    header: {
        login: 'Anmelden',
        register: 'Registrieren',
        settings: 'Einstellungen',
        team: 'Team',
        subscription: 'Abonnement',
        help: 'Hilfe',
        logout: 'Abmelden'
    },
    ticket: {
        tasks: 'Meine Tasks für heute',
        no_tasks: 'Keine Tasks für heute 😊',
        create: 'Ticket erstellen',
        edit: 'Ticket bearbeiten',
        delete: 'Ticket löschen',
        save: 'Ticket speichern',
        delete_title: 'Ticket löschen',
        delete_message: 'Möchten Sie wirklich "{name}" löschen?',
        empty: 'Keine Tickets bisher. Erstellen Sie doch eins.',
        private: 'Privates Ticket',
        private_tooltip: 'Nur Sie können dieses Ticket sehen',
        critical: '{name} enthält Einträge, die heute fällig sind!',
        warning: '{name} enthält Einträge, die diese Woche fällig sind!',
        items: 'Einträge in {name}',
        placeholder: {
            title: 'Ticket Titel'
        }
    },
    item: {
        create: 'Eintrag erstellen',
        actions: 'Aktionen anzeigen…',
        edit: 'Eintrag bearbeiten',
        delete: 'Eintrag löschen',
        close: 'Eintrag unerledigt',
        open: 'Eintrag erledigt',
        save: 'Eintrag speichern',
        delete_title: 'Eintrag löschen',
        delete_message: 'Möchten Sie wirklich "{name}" löschen?',
        empty: 'Keine Einträge in diesem Ticket. Wieso erstellen Sie nicht welche?',
        description: 'Beschreibung',
        no_description: 'Keine Beschreibung.',        
        permalink: 'Permanenter Link zu diesem Eintrag',
        placeholder: {
            title: 'Titel',
            date: 'Bis zum:',
            description: 'Beschreibung',
            assignee: 'Zuweisen an:'
        }
    },
    comment: {
        edit: 'Kommentar bearbeiten',
        updated: '{time} aktualisiert',
        delete: 'Kommentar löschen',
        delete_title: 'Kommentar löschen',        
        delete_message: 'Möchten Sie diesen Kommentar wirklich löschen?',
        count: '{count} Kommentar ::: {count} Kommentare',
        headline: 'Kommentare',
        empty: 'Keine Kommentare bisher.',
        save: 'Kommentar speichern',
        placeholder: 'Kommentar erstellen…'

    },
    settings: {
        application: 'Anwendungs-',
        user: 'Benutzer-',
        advanced: 'Erweiterte',
        critical: 'Tage bis zum "Kritisch" Status',
        warning: 'Tage bis zum "Warnung" Status',
        placeholder: 'Tage',
        language: 'Sprache',
        avatar: 'Profilbild',
        hide: 'Diesen Benutzer in Dropdowns verstecken',
        delete_caches_all: 'Alle Caches löschen',
        delete_caches_app: 'App-Cache löschen',
        delete_caches_api: 'Api-Cache löschen'
    },
    team: {
        button_create: 'Team erstellen',
        button_add: 'Mitglied hinzufügen',
        button_delete: 'Team löschen',
        button_rename: 'Rename Team',        
        owner: 'Eigentümer',
        member: 'Mitglied',
        invited: 'Eingeladen',
        label_create: 'Neues Team erstellen',        
        label_add: 'Neues Team-Mitglied hinzufügen',
        placeholder_name: 'Team-Name',
        placeholder_email: 'E-Mail-Adresse',
        tooltip_delete: 'Team-Mitglied entfernen',
        tooltip_revoke: 'Einladung zurückziehen',
        delete_team_title: 'Team löschen',
        delete_team_body: 'Möchten Sie wirklich "{team}" löschen?',
        delete_member_title: 'Team-Mitglied entfernen',
        delete_member_body: 'Möchten Sie wirklich {name} von "{team}" entfernen?',
        revoke_invitation_title: 'Einladung zurückziehen',
        revoke_invitation_body: 'Möchten Sie wirklich die Einladung von {name} zurückziehen?'
    },
    subscription: {
        users: 'Benutzer',
        month: 'pro Monat',
        year: 'pro Jahr',
        monthly: 'monatliche',
        yearly: 'jährliche',
        payment: 'Zahlung',
        valid_from: 'Gultig ab:',
        valid_until: 'Gültig bis:',
        subscribe: 'Abonnieren',
        cancel: 'Kündigen',
        resume: 'Fortsetzen',
        upgrade: 'Wechseln',
        payment_details: 'Bitte geben Sie Ihre Zahlungsdaten ein:',
        processing: 'Ihre Transaktion wird verarbeitet. Bitte warten',
        messages: {
            active: 'Ihre Zahlung wurde verarbeitet. Ihr Abonnement ist nun aktiv!',
            resumed: 'Ihr Abonnement wurde fortgesetzt.',
            canceled: 'Ihr Abonnement wurde gekündigt.',
            upgraded: 'Ihr Abonnement wurde geändert.'
        }
    },
    help
}