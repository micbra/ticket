export default `
# Usage of Tickets
The concept of the ticket app is basically based on three principles: **Tickets**, **Items** and **Teams**.

## Tickets
Tickets are the top level of the organization. They are used to group multiple items.
For example, create a ticket for each project, customer, etc. that you serve.

### Create ticket
To create a new ticket, click **Create ticket** in the *Tickets* view on your [Dashboard](/tickets).

> You can create as many tickets as you like.

In the following step, enter a name for the ticket.

#### Private Tickets
When creating the ticket, you have the option of marking a ticket as private.
If you check the corresponding checkbox, only you will be able to see the ticket. Your team members only see public tickets!

### Edit ticket
To edit a ticket, such as changing the name or private status, select the ticket and then click the pencil icon in the top right corner.

### Delete ticket
To delete a ticket, select the ticket and click on the trashcan icon in the upper right corner and confirm the following dialog box.

> Attention: If you delete a ticket, all associated items and their comments will be irreversibly deleted!

## Items
Each ticket can contain any number of items.
Items can be, for example, tasks that accrue to your project or upcoming appointments.

### Create an item
To create an item, select a ticket and then click **Create item**.

Each item must have a *title*. Optionally, you can specify an *expiration date*, add a *description*

> The description of an item can be formatted with [Markdown](https://guides.github.com/features/mastering-markdown/).

or assign a *user*.
> If you do not assign a user, the ticket is automatically assigned to you.

### View  item
After creating an item, it will be displayed open by default. You can see the description as well as the comments (or add the latter) and via the chain icon you can get a permanent link to this item.
If you want to close an item or open another one, just click on it's title.

#### Item priority
Items that have set an expiration date will be displayed in green, yellow or red, depending on how far the expiration date is in the future.
By default, all entries whose expiration date is more than one week in the future are displayed in green. If it's less than a week but more than 24 hours in the future, it is displayed in yellow and if it's less than 24 hours in the future or even in the past, the item is highlighted in red.

> You can adjust these default settings in the *Application Settings* area in [Settings](/settings).

### Set an Item to done
If you want to mark an item as completed, click on the check mark of the respective item. It is now displayed with less opacity and moved to the very bottom of the ticket.

If you want to mark the item as open again, simply click on the same button (it will now be yellow with an arrow icon).

### Edit an item
If you would like to change the data of an item or assign it to another person, please click on the pencil icon of the respective item.

### Delete item
To delete an item, click on the trashcan icon of the respective item and confirm the following dialog box.

> Attention: If you delete an item, the data (title, description, etc.) and all associated comments will be irreversibly deleted!

### Comments
Each item can contain as many comments as you like.

> Comments can be formatted with [Markdown](https://guides.github.com/features/mastering-markdown/).

If you are the creator of a comment, you can edit or delete it. Please click on the respective icon next to the comment.

## Teams
Each user can create a team. However, adding members to a team requires upgrading to a paid plan.

> Upgrades can be made on the [Subscription](/subscription) page!

Team members can see all the public tickets of their team and can edit them or create new tickets.
They can also add entries to tickets and edit or delete existing ones. It is also possible to post comments on items.

### Create team
To create a team, simply go to the [Team](/team) page, choose a team name, and click the **Create Team** button.

### Team members
Each team can not have more members than the amount specified in the subscription.

#### Add member
To add a member, enter his e-mail address in the appropriate field and click **Add member**.
An invitation will be sent to the specified e-mail address, which can now be accepted by the recipient.

#### Remove Member / Cancel Invitation
To remove a member or withdraw a sent invitation, please click the trashcan icon next to each member.
> Note that you may be forced to remove team members when you downgrade your subscription plan to one with fewer members than your team has.

&nbsp;

# Known bugs
- If a new user registers from an invitation e-mail, he will not be added to the invited team. The button for accepting the invitation in the e-mail must be clicked again.
- After changing the assigned user of an item, sometimes the old user is still shown as assigned. It helps to reload the page.
- After you activate or change a subscription, the start or end dates are sometimes not displayed. It helps to reload the page.
`