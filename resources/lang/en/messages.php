<?php

return [
    'invitation_sent' => 'The invitation email has been sent',
    'invitation_pending' => 'This email address is already invited to the team.',
    'team_max_users' => 'You are only allowed to have :count member on your team.|You are only allowed to have :count members on your team.',
    'team_upgrade' => 'To add more members please upgrade your plan!',
    'team_too_small' => 'You currently have too many team members to change to this plan. Please remove :count member from your team first!|You currently have too many team members to change to this plan. Please remove :count members from your team first!'    
];