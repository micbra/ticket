<?php

return [
    'subject' => 'Invitation to join Team',  
    'introduction' => 'Hi there,',
    'invitation' => 'You have been invited by **:Name** to join his/her team.',
    'call_to_action' => 'If you already have a **Tickets Account** just click on the button to join **:Team**.',
    'join' => 'Join Team!',
    'no_account_headline' => 'Not an account yet?',
    'no_account_body' => 'To create a new account simply hit the button below. It\'s free!',
    'hint' => 'After your registration you need to click the **Join Team** button to accept the invitation!',    
    'register' => 'Create Account',
    'salutation' => 'Greetings from the Tickets Team!'    
];