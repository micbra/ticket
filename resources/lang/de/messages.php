<?php

return [
    'invitation_sent' => 'Die Einladung wurde per E-Mail versandt.',
    'invitation_pending' => 'An diese E-Mail-Adresse wurde bereits eine Einladung gesendet.',
    'team_max_users' => 'Ihr Team darf maximal :count Mitglied beinhalten.|Ihr Team darf maximal :count Mitglieder beinhalten.',
    'team_upgrade' => 'Bitte upgraden Sie Ihren Tarif um weitere Mitglieder hinzuzufügen!',
    'team_too_small' => 'Sie haben Momentan zu viele Mitglieder in Ihrem Team um zu diesem Tarif zu wechseln. Bitte entfernen Sie erst :count Team-Mitglied!|Sie haben Momentan zu viele Mitglieder in Ihrem Team um zu diesem Tarif zu wechseln. Bitte entfernen Sie erst :count Team-Mitglieder!'
];