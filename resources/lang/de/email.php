<?php

return [
    'subject' => 'Einladung um Team beizutreten',
    'introduction' => 'Hallo,',
    'invitation' => '**:Name** hat sie eingeladen seinem/ihrem Team beizutreten.',
    'call_to_action' => 'Wenn Sie bereits einen **Tickets-Account** haben, klicken Sie auf den Button um sich einzuloggen und **:Team** beizutreten',
    'join' => 'Team beitreten!',
    'no_account_headline' => 'Noch keinen Account?',
    'no_account_body' => 'Klicken Sie einfach auf den untenstehenden Button um sich zu registrieren. Es kostet nichts!',
    'register' => 'Account erstellen',
    'hint' => 'Bitte klicken Sie nach der Registrierung auf den **Team beitreten** Button in dieser E-Mail um die Einladung anzunehmen!',
    'salutation' => 'Viele Grüße vom Tickets Team!'
];