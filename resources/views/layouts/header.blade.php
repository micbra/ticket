<nav class="navbar navbar-light bg-white justify-content-between mb-5">
    <div class="container">
        <a class="navbar-brand" href="/tickets">
            <img class="navbar-image" src="/img/logo.svg" width="30px" height="30px"/>
            Tickets
        </a>
        <ul class="navbar-nav flex-row">
            <li class="nav-item pr-3">
                <a class="nav-link {{ Request::is('login') ? 'active' : '' }}" href="/login">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('register') ? 'active' : '' }}" href="/register">Register</a>
            </li>
        </ul>
    </div>
</nav>