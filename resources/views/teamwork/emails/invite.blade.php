@component('mail::message')
## @lang('email.introduction')

{{ __('email.invitation', ['name' => $invite->inviter->name]) }}

{{ __('email.call_to_action', ['team' => $invite->team->name]) }}
@component('mail::button', ['url' => route('teams.accept_invite', $invite->accept_token)])
@lang('email.join')
@endcomponent

### @lang('email.no_account_headline')

@lang('email.no_account_body')
@component('mail::button', ['url' => route('register')])
@lang('email.register')
@endcomponent

@lang('email.hint')


@lang('email.salutation')
@endcomponent