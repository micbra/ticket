<?php

namespace Tests\Feature;

use App\Ticket;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TicketTest extends TestCase
{
    use RefreshDatabase;

    public $ticket;

    public function setUp()
    {
        parent::setUp();

        $this->ticket = factory(Ticket::class)->create(['user_id' => $this->user->id]);
    }

    /**
     * A signed in user can only view his own private and public tickets.
     *
     * @return void
     */
    public function testUserCanSeeOwnTickets()
    {
        // given there are both own and other tickets
        // and each are both private and public
        $privateTicket = factory(Ticket::class)->states('private')->create(['user_id' => $this->user->id]);
        $otherTicket = factory(Ticket::class)->create(['user_id' => 42]);
        $otherPrivateTicket = factory(Ticket::class)->states('private')->create(['user_id' => 42]);

        // when the user views all tickets
        $response = $this->get('/api/tickets');

        // then only the own ones are shown
        $json = [$this->ticket->toArray(), $privateTicket->toArray()];
        $missing = [$otherTicket->toArray(), $otherPrivateTicket->toArray()];
        
        $response->assertJson($json);
        $response->assertJsonMissing($missing);
    }

    /**
     * A signed in user can create a ticket.
     *
     * @return void
     */
    public function testUserCanCreateTicket()
    {
        // given the user creates some ticket data
        $ticket = factory(Ticket::class)->make(['user_id' => $this->user->id]);
        
        // when the user posts the data to the api
        $response = $this->post('/api/tickets', $json = $ticket->toArray());

        // then the data should be persisted to the database
        $this->assertDatabaseHas('tickets', $json);

        // and the response contains the data
        $response->assertJson($json);
    }

    /**
     * A signed in user can update a ticket.
     *
     * @return void
     */
    public function testUserCanUpdateTicket()
    {
        // given the user has already created a ticket (setUp)

        //when he updates the ticket
        $old_title = $this->ticket->title;
        $this->ticket->title = 'foobar';
        $response = $this->put(
            '/api/tickets/' . $this->ticket->id,
            $json = $this->ticket->toArray()
        );
        
        // then the updated data should be reflected in the database
        $this->assertDatabaseHas('tickets', $json);

        // and being returned by the api
        $response->assertJson($json);
        $response->assertJsonMissing(['title' => $old_title]);
    }

    /**
     * A signed in user can set his own ticket to private.
     *
     * @return void
     */
    public function testUserCanSetOwnTicketToPrivate()
    {
        // given the user has already created a ticket (setUp)

        //when he updates the ticket to be private
        $this->ticket->private = true;
        $response = $this->put(
            '/api/tickets/' . $this->ticket->id,
            $json = $this->ticket->toArray()
        );

        // then the updated data should be reflected in the database
        $this->assertDatabaseHas('tickets', $json);

        // and being returned by the api
        $response->assertJson($json);
    }

    /**
     * A signed in user can not set other tickets to private.
     *
     * @return void
     */
    public function testUserCanNotSetOtherTicketToPrivate()
    {
        //given there is a ticket of a other user
        $ticket = factory(Ticket::class)->create(['user_id' => 42]);

        //when the user tries to set it to private
        $ticket->private = true;
        $response = $this->put(
            '/api/tickets/' . $ticket->id,
            $json = $ticket->toArray()
        );

        //then it should not change
        $this->assertDatabaseMissing('tickets',  $json);
        
        $this->expectException('ErrorException');
        $response->assertJson($json);
    }

     /**
     * A signed in user can see tickets of team mebers.
     *
     * @return void
     */
    public function testUserCanSeeTeamTicket()
    {
        $user = factory(\App\User::class)->create();
        $team = factory(\App\Team::class)->create(['owner_id' => $user->id]);
        $ticket = factory(Ticket::class)->create(['user_id' => $user->id, 'team_id' => $team->id]);
        $user->attachTeam($team);
        $this->user->attachTeam($team);

        $response = $this->get('/api/tickets');

        $response->assertJsonFragment([
            'id' => $ticket->id
        ]);
    }

    /**
     * A signed in user can not see tickets of other teams.
     *
     * @return void
     */
    public function testUserCanNotSeeOtherTeamTicket()
    {
        $user = factory(\App\User::class)->create();
        $team = factory(\App\Team::class)->create(['owner_id' => $user->id]);
        $ticket = factory(Ticket::class)->create(['user_id' => $user->id, 'team_id' => $team->id]);
        $user->attachTeam($team);

        $response = $this->get('/api/tickets');

        $response->assertJsonMissing([
            'id' => $ticket->id
        ]);
    }
}
