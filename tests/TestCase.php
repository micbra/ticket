<?php

namespace Tests;

use App;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $user;

    public function setUp()
    {
        parent::setUp();
        
        $this->user = factory(App\User::class)->create();
        
        $this->actingAs($this->user, 'api');
    }
}
