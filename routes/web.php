<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () { return redirect('/tickets'); })->name('index');

Route::get('/tickets', 'AppController@index')->middleware('auth');
Route::get('/team', 'AppController@index')->middleware('auth');
Route::get('/subscription', 'AppController@index')->middleware('auth');
Route::get('/settings', 'AppController@index')->middleware('auth');
Route::get('/help', 'AppController@index')->middleware('auth');

Route::middleware(['auth'])->prefix('tickets')->group(function () {
    Route::get('{any}', 'AppController@index')->where('any', '.*');
});

Auth::routes();

Route::get('team/accept/{token}', 'Teamwork\AuthController@acceptInvite')->name('teams.accept_invite');
Route::post('stripe/webhook', 'WebhookController@handleWebhook')->middleware('stripe');
