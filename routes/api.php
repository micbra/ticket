<?php

use Illuminate\Http\Request;

use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    $user = $request->user();
    $settings = $user->settings;
    return $user;
});

Route::apiResource('team', 'Api\TeamController')->middleware('auth:api');
Route::delete('/team/{team}/member/{user}', 'Api\TeamController@deleteMember')->middleware('auth:api');
Route::delete('/team/{team}/invite/{user}', 'Api\TeamController@revokeInvitation')->middleware('auth:api');

Route::apiResource('subscription', 'Api\SubscriptionController')->middleware('auth:api');

Route::apiResource('tickets.items.comments', 'Api\CommentController')->middleware('auth:api');
Route::apiResource('tickets.items', 'Api\ItemController')->middleware('auth:api');
Route::apiResource('tickets', 'Api\TicketController')->middleware('auth:api');

Route::get('/settings', 'Api\SettingsController@index')->middleware('auth:api');
Route::put('/settings', 'Api\SettingsController@update')->middleware('auth:api');
Route::post('/settings', 'Api\SettingsController@avatarAdd')->middleware('auth:api');
Route::delete('/settings', 'Api\SettingsController@avatarDelete')->middleware('auth:api');

Route::get('/version', 'Api\VersionController@index');//->middleware('auth:api');

