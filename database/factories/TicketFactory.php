<?php

use Faker\Generator as Faker;

$factory->define(App\Ticket::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(App\User::class)->create()->id;
        },
        'title' => $faker->word,
        'private' => false
    ];
});

$factory->state(App\Ticket::class, 'team', [
    'team_id' => function () {
        return factory(App\Team::class)->create()->id;
    }
]);

$factory->state(App\Ticket::class, 'private', [
    'private' => true
]);
