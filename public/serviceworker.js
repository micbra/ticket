const cacheName = 'ticket'
const dataCacheName = 'ticket-api'
const filesToCache = [
    '/js/manifest.js',
    '/js/vendor.js',
    '/js/app.js',
    '/css/app.css',
    'https://fonts.googleapis.com/css?family=Raleway:400,600',
    '/offline.html',
    '/img/logo.svg'
]

self.addEventListener('install', e => {
    console.log('[ServiceWorker] Install')
    const promise = caches.open(cacheName).then(cache => {
            console.log('[ServiceWorker] Caching app shell')
            return cache.addAll(filesToCache)
        })

    e.waitUntil(promise)
})

self.addEventListener('activate', e => {
    console.log('[ServiceWorker] Activate')
    e.waitUntil(
        caches.keys().then(keyList => {
            return Promise.all(keyList.map(async key => {
                if (key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] Removing old cache', key)
                    return caches.delete(key)
                }
            }))
        })
    )
    return self.clients.claim()
})

self.addEventListener('fetch', e => {
    // console.log('[ServiceWorker] Fetch', e.request.url);
    var dataUrl = '/api'
    if (e.request.url.indexOf(dataUrl) > -1) {
        /*
        * When the request URL contains dataUrl, the app is asking for fresh
        * data. In this case, the service worker always goes to the
        * network and then caches the response. This is called the "Cache then
        * network" strategy:
        * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
        */
        e.respondWith(
            caches.open(dataCacheName).then(cache => {
                return fetch(e.request).then(response => {
                    cache.put(e.request.url, response.clone())
                    return response
                })
            })
        )
    } else {
        /*
        * The app is asking for app shell files. In this scenario the app uses the
        * "Cache, falling back to the network" offline strategy:
        * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
        */
        e.respondWith(
            caches.match(e.request)
                .then(response => response || fetch(e.request))
                .catch(() => {
                    return caches.match('/offline.html')
            })
        )
    }
})
