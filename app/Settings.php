<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['user_id', 'days_critical', 'days_warning', 'language', 'avatar'];

    protected $casts = [
        'days_critical' => 'integer',
        'days_warning' => 'integer',
        'user_hidden' => 'boolean'
    ];

    // protected $appends = ['language'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
