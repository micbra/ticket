<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mpociot\Teamwork\Traits\UserHasTeams;

use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, UserHasTeams, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function items ()
    {
        return $this->hasMany(Item::class);
    }

    public function comments ()
    {
        return $this->hasMany(Comment::class);
    }

    public function settings ()
    {
        return $this->hasOne(Settings::class);
    }

    public function is_hidden()
    {
        return $this->settings->user_hidden;
    }

    public function getUserHiddenAttribute()
    {
        return optional($this->settings)->user_hidden;
    }
}
