<?php

namespace App\Policies;

use App\User;
use App\Ticket;
use Illuminate\Auth\Access\HandlesAuthorization;

class TicketPolicy
{
    use HandlesAuthorization;

    private function myOrTeamsTicket($user, $ticket)
    {
        $myTicket = $ticket->user_id === $user->id;
        $myTeamsTicket = optional($ticket->team())->id === optional($user->currentTeam)->id;

        return $myTicket || $myTeamsTicket;
    }

    /**
     * Determine whether the user can view the ticket.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function view(User $user, Ticket $ticket)
    {
        return $this->myOrTeamsTicket($user, $ticket);
    }

    /**
     * Determine whether the user can create tickets.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, Ticket $ticket)
    {
        return $this->myOrTeamsTicket($user, $ticket);
    }

    /**
     * Determine whether the user can update the ticket.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function update(User $user, Ticket $ticket)
    {
        return $this->myOrTeamsTicket($user, $ticket);
    }

    /**
     * Determine whether the user can delete the ticket.
     *
     * @param  \App\User  $user
     * @param  \App\Ticket  $ticket
     * @return mixed
     */
    public function delete(User $user, Ticket $ticket)
    {
        return $this->myOrTeamsTicket($user, $ticket);
    }
}
