<?php

namespace App\Policies;

use App\User;
use App\Team;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeamPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the team.
     *
     * @param  \App\User  $user
     * @param  \App\Team  $team
     * @return mixed
     */
    public function view(User $user, Team $team)
    {
        return $team->users->contains(function ($item) {
            return $item->id === $user->id;
        });
    }

    /**
     * Determine whether the user can create teams.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return count($user->teams) < 1;
    }

    /**
     * Determine whether the user can update the team.
     *
     * @param  \App\User  $user
     * @param  \App\Team  $team
     * @return mixed
     */
    public function update(User $user, Team $team)
    {
        return $user->isOwnerOfTeam($team);
    }

    /**
     * Determine whether the user can delete the team.
     *
     * @param  \App\User  $user
     * @param  \App\Team  $team
     * @return mixed
     */
    public function delete(User $user, Team $team)
    {
        return $user->isOwnerOfTeam($team);
    }

    /**
     * Determine whether the user can delete the team.
     *
     * @param  \App\User  $user
     * @param  \App\Team  $team
     * @param  \App\User  $input
     * @return mixed
     */
    public function deleteMember(User $user, Team $team, User $input)
    {
        return $user->id !== $input->id && ($user->isOwnerOfTeam($team) || $user->id === $input->id);
    }

    /**
     * Determine whether the user can revoke team invitations.
     *
     * @param  \App\User  $user
     * @param  \App\Team  $team
     * @param  \App\User  $input
     * @return mixed
     */
    public function revokeInvitation(User $user, Team $team)
    {
        return $user->isOwnerOfTeam($team);
    }
}
