<?php

namespace App\Http\Middleware;

use Stripe\WebhookSignature;

use Closure;

class StripeWebhook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('services.stripe.hook.secret')) {
            // throws Stripe\Error\SignatureVerification exception on verification failure
            WebhookSignature::verifyHeader(
                $request->getContent(),
                $request->header('stripe-signature'),
                config('services.stripe.hook.secret'),
                config('services.stripe.hook.tolerance')
            );
        }
        return $next($request);
    }
}
