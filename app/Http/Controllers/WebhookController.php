<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{

    private function teamsizeUpdate($payload)
    {
        $data = $payload['data']['object'];
        $customer = $data['customer'];
        $quantity = $data['quantity'];

        
        $user = User::where('stripe_id', $customer)->firstOrFail();
        $team = $user->teams->first();

        $team->setUsers($quantity);

        return $team;
    }
    /**
     * Handle a Stripe webhook call.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleWebhook(Request $request)
    {
        $payload = json_decode($request->getContent(), true);

        $method = 'handle'.studly_case(str_replace('.', '_', $payload['type']));

        if (method_exists($this, $method)) {
            return $this->{$method}($payload);
        } else {
            return $this->missingMethod();
        }
    }

    public function handleCustomerSubscriptionCreated($payload) {
        $team = $this->teamsizeUpdate($payload);

        return response()->json($team, 202);
    }

    public function handleCustomerSubscriptionUpdated($payload) {
        $team = $this->teamsizeUpdate($payload);

        return response()->json($team, 202);
    }
}
