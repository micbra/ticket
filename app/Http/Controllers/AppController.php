<?php

namespace App\Http\Controllers;

use App;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index ()
    {
        $settings = auth()->user()->settings;
        App::setLocale($settings->language);

        return view('app');
    }
}
