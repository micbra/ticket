<?php

namespace App\Http\Controllers\Api;

use App;
use App\Plan;
use App\Team;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function index ()
    {
        $plans = Plan::latest()->get();

        $subscription = auth()->user()->subscriptions->first();

        return response()->json([
            'data' => $subscription,
            'plans' => $plans,
            'apiKey' => config('services.stripe.key')
            ]);
    }

    public function store (Request $request)
    {
        $request->validate([
            'token' => 'required',
            'plan' => 'required:small,medium',
            'interval' => 'required:monthly,yearly'
                        
        ]);

        $token = $request->input('token');
        $plan = $request->input('plan');
        $interval = $request->input('interval');

        $plan = Plan::where('plan', $plan)->where('interval', $interval)->firstOrFail();

        $subscription = auth()->user()->newSubscription($plan->product_id, $plan->plan_id)->quantity($plan->users_included)->create($token);
        
        $user = auth()->user();
        if (count($user->teams) < 1) {
            $team = Team::create([
                'name' => 'Team ' . $user->name,
                'owner_id' => $user->id,
                'max_users' => 1
            ]);

            $user->attachTeam($team);
        }

        return response()->json($subscription);
    }

    public function update (Request $request) {
        $user = auth()->user();
        $plan = $request->input('plan');
        $interval = $request->input('interval');

        if ($plan && $interval) {
            $plan = Plan::where('plan', $plan)->where('interval', $interval)->firstOrFail();

            $team = $user->teams->first();
            if (count($team->users) > $plan->users_included) {
                $settings = auth()->user()->settings;
                App::setLocale($settings->language);

                return response()->json([
                    'type' => 'error',
                    'message' => __('messages.team_too_small', ['count' => count($team->users) - $plan->users_included])
                ], 406);
            }

            $subscription = $user->subscriptions->first();
            $subscription->swap($plan->plan_id)->updateQuantity($plan->users_included);
        } else {
            // resume plan
            $subscription = $user->subscriptions->first();
            $subscription->resume();
        }

        return response()->json($subscription);        
    }

    public function destroy () {
        $subscription = auth()->user()->subscriptions->first();
        
        $subscription->cancel();

        return response()->json($subscription);
    }
    
}
