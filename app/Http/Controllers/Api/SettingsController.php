<?php

namespace App\Http\Controllers\Api;

use App\Settings;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Settings $settings)
    {
        $response = auth()->user()->settings;
        return response()->json($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $settings)
    {
        $request->validate([
            'warning' => 'required',
            'critical' => 'required',
            'language' => 'required',
            'hidden' => 'required',
            //'avatar' => 'required'
        ]);
        
        $settings = auth()->user()->settings;

        $settings->days_warning = $request->input('warning');
        $settings->days_critical = $request->input('critical');
        $settings->language = $request->input('language');
        $settings->user_hidden = $request->input('hidden');
        $settings->avatar = $request->input('avatar');
    

        $settings->save();

        return response()->json($settings);
    }



    public function avatarAdd(Request $request)
    {
        // store the original image
        $path = $request->file('avatar')->store('avatars');

        //get the stored file
        $contents = Storage::get($path);

        // crop the file
        $image = \Image::make($contents)->fit(300, 300, function($c) { $c->upsize(); });

        //save the new file
        $image->save(storage_path('app/public/') . $path);

        //delete the original file
        Storage::delete($path);
        
        //return asset path 
        return response()->json([
            'avatar' => asset('storage/' . $path)
        ]);
    }


    public function avatarDelete(Request $request)
    {
        $settings = auth()->user()->settings;
        $url = $settings->avatar;

        // get relevant part from url
        $path = explode('/', $url , 5)[4];

        // delete image
        Storage::delete('public/' . $path);

        // remove database entry
        $settings->avatar = null;
        $settings->save();
    }
    
}
