<?php

namespace App\Http\Controllers\Api;

use App\Ticket;
use App\Item;
use App\Comment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VersionController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $js = filemtime(public_path('js/app.js'));
        $css = filemtime(public_path('css/app.css'));
        return md5($css . '|' . $js);
    }
}
