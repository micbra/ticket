<?php

namespace App\Http\Controllers\Api;

use App;
use App\Team;
use App\User;
use App\Mail\Invite;

use Illuminate\Support\Facades\Mail;
use Mpociot\Teamwork\Facades\Teamwork;
use Mpociot\Teamwork\TeamInvite;
use Illuminate\Support\Facades\Gate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    private $id;

    private function teamInvites ($team)
    {
        return $team->invites->map(function($invite) {
                    return [
                        'id' => $invite->id,
                        'email' => $invite->email
                    ];
                });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = auth()->user()->teams->map(function ($team) {
            return collect($team)->merge([
                'users' => $team->userData(),
                'invited' => $this->teamInvites($team)
            ]);
        });

        return response()->json($teams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Team::class);
        
        $request->validate([
            'name' => 'required'
        ]);

        $team = Team::create([
            'name' => $request->input('name'),
            'owner_id' => auth()->user()->id,
            'max_users' => 1
        ]);

        auth()->user()->attachTeam($team);

        $team = collect($team)->merge([
            'users' => $team->users
        ]);

        return response()->json($team);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        $this->authorize('view', $team);

        $team = collect($team)->merge([
            'users' => $team->users,
            'invited' => $this->teamInvites($team)
        ]);
        
        return response()->json($team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {   
        $this->authorize('update', $team);

        $settings = auth()->user()->settings;
        App::setLocale($settings->language);

        $email = $request->input('mail');

        if ($email) {
            if($team->users->count() >= $team->max_users) {
                return response()->json([
                    'success'=> false,
                    'message' => trans_choice('messages.team_max_users', $team->max_users, ['count' => $team->max_users]) . ' ' . __('messages.team_upgrade')
                ]);
            }

            if (!Teamwork::hasPendingInvite($email, $team)) {
                Teamwork::inviteToTeam($email, $team, function($invite) {
                    Mail::to($invite->email)->send(new Invite($invite));
                    $this->id = $invite->id;
                });

                return response()->json([
                    'success' => true,
                    'message' => __('messages.invitation_sent'),
                    'id' => $this->id || null
                ]);
            } else {
                return response()->json([
                    'success'=> false,
                    'message' => __('messages.invitation_pending')
                ]);
            }
        } else {
            $team->name = $request->input('name');
            $team->save();

            return response()->json($team);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $this->authorize('delete', $team);

        User::where('current_team_id', $team->id)->update(['current_team_id' => null]);
        $team->delete();

       return response()->json($team);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deleteMember(Team $team, User $user)
    {
        $this->authorize('deleteMember', [$team, $user]);
        $user->detachTeam($team);
        return response()->json($team->users);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function revokeInvitation(Team $team, TeamInvite $user)
    {
    $this->authorize('revokeInvitation', $team);
        
      $user->delete();

      return response()->json($this->teamInvites($team));
    }
}
