<?php

namespace App\Http\Controllers\Api;

use App\Ticket;
use App\Item;
use App\Team;

use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    private function addStatus($ticket)
    {
        $ticket->hasCritical = $ticket->hasCritical();
        $ticket->hasWarning = $ticket->hasWarning();
        return $ticket;
    }

    private function myAndTeamTickets($query) {
        $team = auth()->user()->teams->first();
        
        if($team) {
            $userIds = $team->users->pluck('id')->toArray();

            $query->whereIn('user_id', $userIds)
                  ->orWhere('user_id', auth()->user()->id);
        } else {
            $query->where('user_id', auth()->user()->id);
        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicTickets = Ticket::with(['items', 'items.comments'])
                    ->where('private', false)
                    ->where(function ($query) {
                        $this->myAndTeamTickets($query);
                    })
                    ->latest()
                    ->get()
                    ->transform(function($ticket) { return $this->addStatus($ticket); });

        $privateTickets = Ticket::with(['items', 'items.comments'])
                    ->where('private', true)
                    ->where('user_id', auth()->user()->id)
                    ->latest()
                    ->get()
                    ->transform(function($ticket) { return $this->addStatus($ticket); });

        $tickets = $publicTickets->merge($privateTickets)->sortBy('id')->values();

        return response()->json($tickets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'private' => 'required'
        ]);

        
        $ticket = Ticket::create([
            'user_id' => auth()->user()->id,
            'title' => $request->input('title'),
            'private' => $request->input('private')
        ]);
            
        return response()->json($ticket);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {  
        $data = Ticket::where('id', $ticket->id)
                        ->where(function ($query) {
                            $this->myAndTeamTickets($query);
                        })
                        ->with(['items', 'items.comments'])
                        ->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $this->authorize('update', $ticket);

        $request->validate([
            'title' => 'required',
            'private' => 'required'
        ]);
        
        $ticket->title = $request->input('title');
        
        if (Gate::allows('set-private', $ticket)) {
            $ticket->private = $request->input('private');
        }

        $ticket->save();
        return response()->json($ticket);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $this->authorize('delete', $ticket);
        
        foreach ($ticket->items as $i => $item) {
            foreach ($item->comments as $j => $comment) {
                $comment->delete();
            }
            $item->delete();
        }
        $ticket->delete();

        return response()->json($ticket);

    }
}
