<?php

namespace App\Http\Controllers\Api;

use App\Item;
use App\Ticket;
use App\User;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Ticket $ticket)
    {
        $this->authorize('view', $ticket);

        return response()->json($ticket->items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Ticket $ticket)
    {  
        $this->authorize('create', $ticket);

        $request->validate([
            'title' => 'required',
            'due_date' => 'nullable|date'
        ]);

        $userId = $request->input('assignee') ? $request->input('assignee') : auth()->user()->id;
        $user = User::find($userId);

        if (Gate::allows('select-user', $user)) {
            $item = Item::create([
                'title' => $request->input('title'),
                'due_date' => $request->input('due_date'),
                'description' => $request->input('description'),
                'user_id' => $userId,
                'ticket_id' => $ticket->id,
                'done' => false
            ]);

            return response()->json($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket, Item $item)
    {
        $this->authorize('view', $item);
        
        return response()->json($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket, Item $item)
    {
        $this->authorize('update', $item);

        $userId =  $request->input('assignee');
        $user = User::find($userId);

        if ($user && Gate::allows('select-user', $user)) {
            $item->title = $request->input('title');
            $item->due_date = $request->input('due_date');
            $item->description = $request->input('description');
            $item->user_id = $request->input('assignee');
        } elseif ($request->has('done')) {
            $item->done = $request->input('done');
        } else {
            abort(403);
        }
     
        $item->save();
        return response()->json($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket, Item $item)
    {
        $this->authorize('delete', $item);

        foreach ($item->comments as $key => $comment) {
            $comment->delete();
        }

        $item->delete();

        return response()->json($item);
    }
}
