<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Item;
use App\Ticket;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Ticket $ticket, Item $item)
    {
        $this->authorize('view', $ticket);
        
        return response()->json($item->comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Ticket $ticket, Item $item)
    {
        $this->authorize('create', $ticket);

        $request->validate([
            'body' => 'required'
        ]);

        $comment = Comment::create([
            'user_id' => auth()->user()->id,
            'item_id' => $item->id,
            'body' => $request->input('body')
        ]);

        return response()->json($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket, Item $item, Comment $comment)
    {
        $this->authorize('view', $ticket);        
        
        return response()->json($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket, Item $item, Comment $comment)
    {
        $this->authorize('update', $comment);

        $request->validate([
            'body' => 'required'
        ]);

        $comment->body = $request->input('body');
        $comment->save();
        
        return response()->json($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket, Item $item, Comment $comment)
    {
        $this->authorize('delete', $comment);
        
        $comment->delete();
        return response()->json($comment);
    }
}
