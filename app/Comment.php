<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'item_id', 'body'];

    protected $appends = ['username'];


    public function getUsernameAttribute()
    {
        return $this->user()->first()->name;
    }

    public function item ()
    {
        return $this->belongsTo(Item::class);
    }

    public function user ()
    {
        return $this->belongsTo(User::class);
    }
}
