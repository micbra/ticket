<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
   protected $fillable = ['title', 'user_id', 'private'];

   protected $casts = [
       'private' => 'boolean'
   ];

   public function path ()
   {
       return '/tickets/' . $this->id;
   }

   public function items ()
   {
       return $this->hasMany(Item::class)
                    ->orderBy('done')
                    ->orderBy('due_date');
   }

   public function team ()
   {
       return User::find($this->user_id)->currentTeam;
   }

   public function hasCritical ()
   {
       return $this->items->contains(function ($item, $key) {
           return $item->priority() === 'critical' && $item->done === false;
       });
   }

   public function hasWarning ()
   {
       return $this->items->contains(function ($item, $key) {
           return $item->priority() === 'high' && $item->done === false;
       });
   }
}