<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Ticket' => 'App\Policies\TicketPolicy',
        'App\Item' => 'App\Policies\ItemPolicy',
        'App\Comment' => 'App\Policies\CommentPolicy',
        'App\Team' => 'App\Policies\TeamPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('fetch-user', function ($user, $input) {
            return !$input->user_hidden && optional($input->currentTeam)->id === optional($user->currentTeam)->id;
        });

        Gate::define('select-user', function ($user, $input) {
            return optional($input->currentTeam)->id === optional($user->currentTeam)->id || $input->id === $user->id;
        });

        Gate::define('set-private', function ($user, $ticket) {
            return $ticket->user_id == $user->id;
        });
    }
}
