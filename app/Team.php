<?php namespace App;

use App\User;

use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{
    protected $fillable = ['name', 'owner_id', 'max_users'];

    public function userData()
    {
        return User::where('current_team_id', optional(auth()->user()->currentTeam)->id)->with('settings')->get();
    }

    public function setUsers($count)
    {
        $this->max_users = $count;
        $this->save();
    }
}