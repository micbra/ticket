<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{   
    protected $casts = [
        'activated' => 'date',
        'expires' => 'date',
    ];
    
    protected $appends = ['is_active', 'is_canceled', 'activated', 'expires'];
   
    public function getIsActiveAttribute()
    {
        return $this->isActive();
    }

    public function getIsCanceledAttribute ()
    {
        return $this->isActive() && $this->expires();
    }

    public function getActivatedAttribute () {
        if ($this->isActive()) {
            return $this->activated();
        } else {
            return null;
        }
    }

   public function getExpiresAttribute () {
        if ($this->isActive()) {
            return $this->expires();
        } else {
            return null;
        }
    }

    public function isActive ()
    {
        $subscription = auth()->user()->subscriptions->first();

        $subscription = optional($subscription);

        $isProduct = $subscription->name === $this->product_id;
        $isPlan = $subscription->stripe_plan === $this->plan_id;
        $isQuantity = $subscription->quantity === $this->users_included;
        
        return $isProduct && $isPlan && $isQuantity;
    }

    public function activated ()
    {
        $subscription = auth()->user()->subscriptions->first();

        $subscription = optional($subscription);

        return optional($subscription->created_at)->format('d.m.Y');
    }

    public function expires ()
    {
        $subscription = auth()->user()->subscriptions->first();

        $subscription = optional($subscription);

        return optional($subscription->ends_at)->format('d.m.Y');
        
    }
}
