<?php

namespace App;

use App\Settings;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Item extends Model
{
    protected $fillable = ['title', 'user_id', 'ticket_id', 'due_date', 'description', 'notes', 'done'];

    protected $casts = [
        'done' => 'boolean'
    ];

    protected $dates = ['due_date'];

    protected $appends = ['priority', 'username'];


    public function getPriorityAttribute()
    {
        return $this->priority();
    }

    public function getUsernameAttribute()
    {
        return $this->user()->first()->name;
    }


    public function ticket ()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function comments ()
    {
        return $this->hasMany(Comment::class);
    }

    public function path ()
    {
        return $this->ticket->path() . '/items/' . $this->id;
    }

    public function priority ()
    {
        $priority = 'normal';
        $today = new Carbon();

        if (!is_null($this->due_date)) {
            $critical_days = Settings::first()->days_critical;
            $warning_days = Settings::first()->days_warning;

            if ($this->due_date->subDays($critical_days) <= $today) {
                $priority = 'critical';
            } elseif ($this->due_date->subDays($warning_days) <= $today) {
                $priority = 'high';
            }
        }
        
        return $priority;
    }
}
